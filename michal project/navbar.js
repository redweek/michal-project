document.getElementById('navbar').innerHTML = `
<nav class="navbar">
    <img class="logo" src="https://beyond.hackampus.com/canvas/general/general-cjQAi-2021.02.08.08.09.59-logo2021.png">
    <div class="navbar-links">
        <ul>
            <li><a href="./home.html">Home</a></li>
            <li><a href="./about.html">About</a></li>
            <li><a href="./media.html">Media</a></li>
            <li><a href="./faq.html">FAQ</a></li>
            <li><a href="./contact.html">Contact</a></li>
        </ul>
    </div>    
    <a href="#" class="hamburger">
        <span class="bar"></span>
        <span class="bar"></span>
        <span class="bar"></span>
    </a>
</nav>
`;

const hamburger = document.getElementsByClassName('hamburger')[0];
const navbarLinks = document.getElementsByClassName('navbar-links')[0];

hamburger.addEventListener('click', () => {
    console.error('ok')
    navbarLinks.classList.toggle('active')
});
